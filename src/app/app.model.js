define(function (require) {
    "use strict";
    
    var angular = require("angular");
    
    function CoreModel () {
        var _extend = {};
        
        function Model (object) {
            this.init(object);
        }
     
        Model.prototype.init = function (object) {
            angular.extend(this, _extend);
            
            if (angular.isObject(object)) {
                angular.extend(this, object);
            }
        };
        
        Model.extend = function (object) {
            if (angular.isObject(object)) {
                _extend = object;
            }
            
            return Model;
        };
        
        return Model;
    }
    
    
    return CoreModel;
});