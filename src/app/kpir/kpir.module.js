define(function (require) {
    var angular = require("angular"),
        ui = require("angular-ui"),
        KpirService = require("kpir/kpir.service"),
        KpirConfig = require("kpir/kpir.config"),
        KpirController = require("kpir/kpir.controller");
    
    angular
            .module("App.KPiR",["ui.bootstrap"])
            .config(KpirConfig)
            .factory("kpirservice", KpirService)
            .controller("KpirController", KpirController);
});