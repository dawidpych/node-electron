define(function (require) {
    function KpirService ($http) {
        var url = 'rest/kpir.json';
        
        return {
            get: get
        };
        
        function get () {
            return $http.get(url);
        }
    }
    
    KpirService.$inject = ["$http"];
    
    return KpirService;
});

