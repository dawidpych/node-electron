define(function () {
   function AppKpirController (kpirservice) {
       var self = this;
       
       self.items = [];
       
       init();
       
       function init() {
           kpirservice.get().then(function (response) {
               self.items = response.data.data.kpir;
           });
       }
   } 
   
    AppKpirController.$inject = ["kpirservice"];
   
   return AppKpirController;
});