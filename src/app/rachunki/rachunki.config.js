define(function () {
    
    function RachunkiConfig ($routeProvider) {
        
        $routeProvider
                .when("/rachunki", {
                    templateUrl: "app/rachunki/views/index.view.html"
                })
                .when("/rachunki/invoice", {
                    templateUrl: "app/rachunki/views/invoice.view.html"
                });
    }
    
    RachunkiConfig.$inject = ["$routeProvider"];
    
    return RachunkiConfig;
});