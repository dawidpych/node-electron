define(function (require) {
    
    var angular = require("angular");
    
    function RachunekModel (AppModel) {  
        var Rachunek = AppModel.extend({
            products: [],
            vendor: {},
            buyer: {},
            type: "RECIVE",
            state: "NEW",
            
            addProduct: function (product) {
                this.products.push(product);
            },
            
            getProduct: function getProduct (id) {
                return angular.copy(this.products[id]);
            },
            
            updateProduct: function (product, id) {
                this.products[id] = product;
            },
            
            removeProduct: function (id) {
                this.products.splice(id, 1);
            }
        });
     
        Rachunek.Type = {
            PEYMENT: "PEYMENT",
            RECIVE: "RECIVE"
        };
        
        Rachunek.State = {
            NEW: "NEW",
            PAID: "PAID",
            UNPAID: "UNPAID",
            CANCEL: "CANCEL"
        };
        
        return Rachunek;
    }
    
    RachunekModel.$inject = ["AppModel"];
    
    return RachunekModel;
});