define(function (require) {
    "use strict";
    
    var angular = require("angular");
    
    function VendorModel (AppModel) {  
        var Vendor = AppModel.extend({
            name: "",
            address: "",
            zip: "",
            city: "",
            nip: "",
            regon: "",
            country: "",
            bankName: "",
            bankAccount: "",
            type: "SELLER" 
        });
        
        Vendor.Type = {
            BUYER: "BUYER",
            SELLER: "SELLER",
            VENDOR: "VENDOR"
        };
        
        return Vendor;
    }
    
    VendorModel.$inject = ["AppModel"];
    
    return VendorModel;
});