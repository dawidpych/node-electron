define(function (require) {
    
    function ProductModel (AppModel) {  
        
        var Product = AppModel.extend({
            name: "",
            kpwir: "",
            unit: "",
            price_netto: "",
            ilosc: "",
            vat: 0,
            cena_brutto: 0
        });
        
        return Product;
    }
    
    ProductModel.$inject = ["AppModel"];
    
    return ProductModel;
});