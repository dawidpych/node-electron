define(function(require) {
    var angular = require("angular"),
        RachunkiConfig = require("rachunki/rachunki.config"),
        RachunkiController = require("rachunki/controllers/rachunki.controller"),
        InvoiceController = require("rachunki/controllers/invoice.controller"),
        ProductModel = require("rachunki/models/product.model"),
        RachunekModel = require("rachunki/models/rachunek.model"),
        VendorModel = require("rachunki/models/vendor.model");
    
    angular
            .module("App.Rachunki",["ngRoute"])
            .config(RachunkiConfig)
            .factory("ProductModel", ProductModel)
            .factory("RachunekModel", RachunekModel)
            .factory("VendorModel", VendorModel)
            .controller("InvoiceController", InvoiceController)
            .controller("RachunkiController", RachunkiController);
});