define(function (require) {
    
    var angular = require("angular");
    
    function RachunkiController (ProductMdel, RachunekModel, VendorModel) {
        var self = this;
            
        self.rachunek = new RachunekModel();
        self.checkbox = [];
        self.addRow = false;
        self.editRow = false;
        self.form = {};
        self.insertProduct = insertProduct;
        self.editProduct = editProduct;
        self.saveProduct = saveProduct;
        self.removeProducts = removeProducts;
        self.isChecked = isChecked;
        
        init();
        
        function init () {
            clearForm();
            self.rachunek.vendor = new VendorModel();
        }
        
        function insertProduct () {
            self.rachunek.addProduct(self.form);
            
            clearForm();
            
            self.editRow = false;
            self.addRow = false;
        }
        
        function editProduct () {
            var id = self.checkbox.indexOf(RachunkiController.State.CHECKED);
            
            self.editRow = true;
            self.addRow = false;
            
            self.form = self.rachunek.getProduct(id);
        }
        
        function saveProduct () {
            var id = self.checkbox.indexOf(RachunkiController.State.CHECKED);
            
            self.editRow = false;
            
            self.rachunek.updateProduct(self.form, id);
            self.checkbox = [];
            
            clearForm();
        }
        
        function removeProducts () {
            angular.forEach(self.checkbox, function (value, key) {
                if (value === RachunkiController.State.CHECKED) {
                    self.rachunek.removeProduct(key);
                } 
            });
            
            self.checkbox = [];
        }
        
        function clearForm () {
            self.form = new ProductMdel();
        }
        
        function isChecked () {
            return self.checkbox.indexOf(RachunkiController.State.CHECKED) > -1;
        }
    }
    
    RachunkiController.State = {
        "CHECKED": true
    };
    
    RachunkiController.$inject = ["ProductModel", "RachunekModel", "VendorModel"];
    
    return RachunkiController;
});