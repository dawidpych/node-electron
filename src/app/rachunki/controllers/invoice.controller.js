define(function (require) {
    
    function InvoiceController (RachunekModel, VendorModel) {
        var self = this;
        
        self.rachunek = new RachunekModel();
        self.rachunek.vendor = new VendorModel({
            type: VendorModel.Type.SELLER
        });
        self.rachunek.buyer = new VendorModel({
            type: VendorModel.Type.BUYER
        });
    }
    
    InvoiceController.$inject = ["RachunekModel", "VendorModel"];
    
    return InvoiceController;
});