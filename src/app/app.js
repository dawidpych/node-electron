define([
    "angular",
    "app.main.controller",
    "app.model",
    "table/table.module",
    "menus/menus.module",
    "rachunki/rachunki.module",
    "kpir/kpir.module"
], function(
        angular, 
        MainController,
        AppModel) {
    
    angular
            .module("App",["ui.bootstrap", "App.Table", "App.Menu", "App.Rachunki", "App.KPiR"])
            .factory("AppModel", AppModel)
            .controller("MainController", MainController);

});