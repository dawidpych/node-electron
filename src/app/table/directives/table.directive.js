define(function(require) {
    "use strict";
    
    var angular = require("angular");
    
    function AppTableDirective () {
        var options = {
            header : [
                    {
                        name: "Lp.",
                        show: true
                    }
                ]
        };

        var directive = {
            restrict: "AE",
            replace: true,
            templateUrl: "app/table/templates/table.html",
            scope: {
                rows: "=data",
                options: "=options"
            },
            link: link
        };
        
        return directive;
        
        function link(scope) {
            if (!scope.rows) {
                scope.rows = [];
            }

            if(scope.options) {

            } else {
                angular.forEach(scope.rows[0], function (value, key) {
                    this.push({name: key});
                }, options.header);
            }

            scope.header = options.header;
        }
    };
    
    return AppTableDirective;
});