define([
    "angular", 
    "table/directives/table.directive"
    ], function(
            angular,
            TableDirective) {
    "use strict";
    
    angular
            .module("App.Table",[])
            .directive("appTable", TableDirective)
            .directive("appTr", function () {
                return {
                    template: "<tr><td>test</td></tr>"
                };
            });
});