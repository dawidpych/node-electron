define(function() {
   
    function AppMenuDirective (menuservice) {
        var directive =  {
            restrict: "AE",
            templateUrl: "app/menus/views/menus.view.html",
            scope: {
                classes: '='
            },
            replace: true,
            link: link
        };

        return directive;

        function link (scope) {
            scope.menus = [];

            menuservice.get().then(function (response) {
                scope.menus = response.data.data.menus;
            }); 
        }
    }
    
    AppMenuDirective.$inject = ["menuservice"];
    
    return AppMenuDirective;
});