define(function(require) {
    function AppMenuDropdown () {
        var directive =  {
            restrict: "AE",
            templateUrl: "app/menus/views/dropdown.view.html",
            scope: {
                menus: '='
            },
            replace: true
        };

        return directive;
    }
    
    return AppMenuDropdown;
});