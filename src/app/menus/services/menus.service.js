define(function() {
    function MenusService ($http) {
        return {
            get: get
        };
        
        function get() {
            return $http.get("rest/menus.json");
        }
    }
    
    MenusService.$inject = ["$http"];
    
    return MenusService;
});