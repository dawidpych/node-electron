define(function(require) {
    var angular = require("angular"),
        ui = require("angular-ui"),
        MenuService = require("menus/services/menus.service"),
        AppMenuDropdown = require("menus/directives/dropdown.directive"),
        AppMenuHeader = require("menus/directives/menus.directive");
    
    angular
            .module("App.Menu",["ui.bootstrap"])
            .factory("menuservice", MenuService)
            .directive("appMenuDropdown", AppMenuDropdown)
            .directive("appMenu", AppMenuHeader);
});