"use strict";

var tt = require("remote");

requirejs.config({
    baseUrl: "app",
    
    nodeRequire: require,
    
    paths: {
        "jquery": "../assets/js/jquery/jquery.min",
        "angular": "../assets/js/angularjs/angular.min",
        "angular-ui": "../assets/js/angularui/ui-bootstrap-tpls.min",
        "ngRoute": "../assets/js/angularjs/angular-route.min"
    },
     
    shim:{
        "angular": {
            exports: "angular"
        },
        "angular-ui": {
            deps: ["angular"]
        },
        "ngRoute": {
            deps: ["angular"]
        }
    },
    
    priority:[
        "jquery",
        "angular"
    ]
});

requirejs([
    "angular",
    "jquery",
    "angular-ui",
    "ngRoute",
    "app",
    "table/table.module"
], function (angular) {
    angular.bootstrap(document, ["App"]);
});